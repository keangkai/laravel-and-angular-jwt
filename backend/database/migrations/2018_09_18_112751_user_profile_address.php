<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserProfileAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profile_address', function (Blueprint $table) {
            $table->increments('id');
            $table->foreign('country_id');
            $table->foreign('province_id');
            $table->foreign('district_id');
            $table->foreign('sub_district_id');
            $table->foreign('postal_code_id');
            $table->foreign('user_profile_id');
            $table->string('address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profile_address');
    }
}
