<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StoreAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_address', function (Blueprint $table) {
            $table->increments('id');
            $table->foreign('country_id');
            $table->foreign('province_id');
            $table->foreign('district_id');
            $table->foreign('sub_district_id');
            $table->foreign('postal_code_id');
            $table->foreign('store_id');
            $table->string('address');
            $table->float('lat');
            $table->float('lng');
            $table->string('address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_address');
    }
}
