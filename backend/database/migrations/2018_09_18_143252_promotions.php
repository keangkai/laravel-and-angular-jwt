<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Promotions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotions', function (Blueprint $table) {
            $table->increments('id');
            $table->foreign('store_category_id');
            $table->string('name');
            $table->string('description');
            $table->string('slug');
            $table->string('tag');
            $table->integer('path_profile_social');
            $table->tinyInteger('active');
            $table->integer('quantity');
            $table->float('price');
            $table->integer('view');
            $table->text('detail');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions');
    }
}
