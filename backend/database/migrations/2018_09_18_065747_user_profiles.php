<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserProfiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->foreign('user_id');
            $table->foreign('name_title_id');
            $table->foreign('nationality_id');
            $table->foreign('ethnicity_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('online_name');
            $table->integer('id_card');
            $table->string('phone_number');
            $table->string('detail');
            $table->string('description');
            $table->string('keywords');
            $table->string('slug');
            $table->string('tag');
            $table->string('logo');
            $table->tinyInteger('active');
            $table->integer('view');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
