<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Stores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->foreign('type_store_id');
            $table->foreign('user_id');
            $table->string('name');
            $table->string('logo');
            $table->string('description');
            $table->string('keywords');
            $table->string('slug');
            $table->integer('followers');
            $table->integer('following');	
            $table->string('tag');
            $table->integer('path_profile_social');
            $table->tinyInteger('active');
            $table->integer('view');
            $table->text('detail');
            $table->integer('view');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
