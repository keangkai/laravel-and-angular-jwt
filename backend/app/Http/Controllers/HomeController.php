<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class HomeController extends Controller
{
    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */
    public function index(){
        $data = [
            "companyDefect"=> [
            "id"=> 29,
            "defect_title"=> "ห้อง 103 โถสุขภัณฑ์ชำรุด",
            "defect_detail"=> null,
            "defect_deadline"=> "2018-06-21",
            "report_company"=> "บริษัท กาญจน์กนก พร็อพเพอร์ตี้ จำกัด",
            "report_user_name"=> "บริษัท กาญจน์กนก พร็อพเพอร์ตี้ จำกัด",
            "report_user_position"=> null,
            "report_company_address"=> "บริษัท กาญจน์กนก พร็อพเพอร์ตี้ จำกัด 88/8 หมู่ 2 ต.หนองผึ้ง อ.สารภี จ.เชียงใหม่ 50140",
            "project_address"=> "สี่แยกทางลอดอุโมงค์บวกครก ถ.สันกำแพงสายเก่า 1006 ตำบล หนองป่าครั่ง อำเภอเมืองเชียงใหม่ เชียงใหม่ 50000 ประเทศไทย",
            "project_name"=> "บิสพอยท์ 1",
            "plan_name"=> "แปลนบิสพอยท์ 1 ชั้น 2",
            "plan_floor"=> 2,
            "status"=> 3
            ],
            "companyDefectPoint"=> [
        
                "name"=> "โถสุขภัณฑ์ชำรุด"
            ],
            "companyDefectImages"=>[
                [
                    "path"=> "https://media.apnarm.net.au/media/images/2018/08/08/b881516116z1_20180808165236_000giq16rsi32-0-on9xvqs6capbfx6vqq2_ct677x380.jpg"
                ]
        
            ],
            "companyDefectManagement"=> [
                [
                    "user_manage_name"=> "บริษัท กาญจน์กนก พร็อพเพอร์ตี้ จำกัด",
                    "detail"=> "ดำเนินการซ่อมวันแรก",
                    "date"=> "2018-06-19 17:35:28",
                    "approv_by"=> null,
                    "image"=> "https://media.apnarm.net.au/media/images/2018/08/08/b881516116z1_20180808165236_000giq16rsi32-0-on9xvqs6capbfx6vqq2_ct677x380.jpg",
                    "start"=> "2018-06-20 08:00:00",
                    "end"=> "2018-06-21 12:00:00"
                ]
                ,
                    [
                    "user_manage_name"=> "บริษัท กาญจน์กนก พร็อพเพอร์ตี้ จำกัด",
                    "detail"=> "ดำเนินการซ่อมวันแรก",
                    "date"=> "2018-06-19 17:35:28",
                    "approv_by"=> null,
                    "image"=> "https://media.apnarm.net.au/media/images/2018/08/08/b881516116z1_20180808165236_000giq16rsi32-0-on9xvqs6capbfx6vqq2_ct677x380.jpg",
                    "start"=> "2018-06-19 17:33:34",
                    "end"=> "2018-06-19 17:36:00"
                    ]
        
            ]
        ];
                return view('home',$data);
            }

    public function generatePDF(){
        $data = ['title' => 'Welcome to HDTuto.com'];

        $pdf = PDF::loadView('myPDF', $data);



        return $pdf->download('hdtuto.pdf');

    }

    public function downloadPDF()

    {

    	$pdf = PDF::loadView('pdfView');

		return $pdf->download('invoice.pdf');

    }
}
