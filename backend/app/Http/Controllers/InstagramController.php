<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Vinkla\Instagram\Instagram;

class InstagramController extends Controller
{
    public function instagramFeed()
    {
        // $instagram = new Instagram();
        // $instagrams = $instagram->get('alamgircsejnu');

        // return view('instagram', compact('instagrams'));

        // Create a new instagram instance.
        $instagram = new Instagram('8582555816.1677ed0.3ffea9a2461a45fb9bcd0376ed2cb1de');

        // Fetch recent user media items.
        $instagram->media();

        // Fetch user information.
        $instagram->self();
    }
}
