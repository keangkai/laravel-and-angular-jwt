<!DOCTYPE html>
<html>
<head>
<style>
* {
    box-sizing: border-box;
}

/* Create four equal columns that floats next to each other */
.column {
    float: left;
    width: 50%;
    padding: 10px;
    height: 170px; /* Should be removed. Only for demonstration */
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}
.flex-container > div {

  margin: 10px;
  padding: 20px;
  font-size: 30px;
}

</style>
</head>
<?php

?>
<div style="border: 2px solid black;border-bottom: white">
    <body style="box-sizing: border-box;">
    <div style="border-bottom: 2px solid black;padding: 5px;">
        <h4 style="text-align:center;">แบบรายงานผลการซ่อมบำรุง</h4>

    <p style="float:right">เลขที่ {{$companyDefect["id"]}}</p>
        <p style="text-decoration:underline">ส่วนที่ 1 ผู้ทำงานซ่อม</p>
        <p> บริษัท / หจก. / นาย / นางสาว / นาง </p>
        <p>ที่อยู่..............</</p>
    </div>


    <div style="border-bottom: 2px solid black;padding: 5px;">
        <p style="text-decoration:underline">ส่วนที่ 2 ผู้รายงาน</p>
        <p> {{$companyDefect["report_user_name"]}}
            @if(!empty($companyDefect["report_user_position"]))
            ({{$companyDefect["report_user_position"]}})
            @endif
            / {{$companyDefect["report_company"]}}</p>

        @if(!empty($companyDefect["report_company_address"]))
            <p >ที่อยู่ {{$companyDefect["report_company_address"]}}</</p>
        @endif

    </div>


    <div style="border-bottom: 2px solid black;padding: 5px;">
        <p style="text-decoration:underline">ส่วนที่ 3 รายละเอียด</p>
        @if(!empty($companyDefect["project_name"]))
        <p>
            {{$companyDefect["project_name"]}}</p>
        @endif
        @if(!empty($companyDefect["project_address"]))
        <p>รายละเอียดงาน : {{$companyDefect["project_address"]}}</p>
        @endif
        <p>วันสิ้นสุดการตรวจสอบ : {{$companyDefect["defect_deadline"]}}</p>
    </div>


    <p style="text-decoration:underline;padding: 5px;">ส่วนที่ 4 ภาพข้อบกพร่อง</p>

    <table>
        @foreach ($companyDefectImages as $value)
        <tr style="border: 1px solid black;text-align:center;justify-content:center;">
                <td style="border: 1px solid black;"><img src="{{$value["path"]}}" alt="{{$value["path"]}}" width="100%" height="50%"></td>
                <td style="border: 1px solid black;"><img src="{{$value["path"]}}" alt="{{$value["path"]}}" width="100%" height="50%"></td>
                <td style="border: 1px solid black;"><img src="{{$value["path"]}}" alt="{{$value["path"]}}" width="100%" height="50%"></td>
                <td style="border: 1px solid black;"><img src="{{$value["path"]}}" alt="{{$value["path"]}}" width="100%" height="50%"></td>
            </tr>
            @endforeach
        </table>

    <br>
</div>

<div style="border: 2px solid black">
    <table style="border: 1px solid black;border-collapse: collapse;width:100%">
            <tr>
              <td colspan="7" style="border: 1px solid black;">ส่วนที่ 5 การดำเนินงาน</td>
            </tr>
            <tr style="border: 1px solid black;text-align:center">
              <td style="border: 1px solid black;">ลำดับ</td>
              <td style="border: 1px solid black;">จุดที่ซ่อม</td>
              <td style="border: 1px solid black;">วันที่เริ่ม</td>
              <td style="border: 1px solid black;">วันที่สิ้นสุด</td>
              <td style="border: 1px solid black;">ผู้รับผิดชอบ</td>
              <td style="border: 1px solid black;">รูปภาพ</td>
              <td style="border: 1px solid black;">หมายเหตุ</td>
            </tr>
            @foreach ($companyDefectManagement as $value)
            <tr style="border: 1px solid black;text-align:center">
                    <td style="border: 1px solid black;"></td>
                    <td style="border: 1px solid black;">{{$value["detail"]}}</td>
                    <td style="border: 1px solid black;">{{$value["start"]}}</td>
                    <td style="border: 1px solid black;">{{$value["end"]}}</td>
                    <td style="border: 1px solid black;">{{$value["user_manage_name"]}}</td>
                    <td style="border: 1px solid black;"><img src="{{$value["image"]}}" alt="{{$value["image"]}}" width="100px" height="50px"></td>
                    <td style="border: 1px solid black;"></td>

                </tr>
                @endforeach
          </table>

          <br>

          {{-- <table style="border: 1px solid black;border-collapse: collapse;width:100%">
                <tr>
                  <td colspan="5" style="border: 1px solid black;">ส่วนที่ 6 รายงานกาารซ่อมบำรุง</td>
                </tr>
                <tr style="border: 1px solid black;text-align:center">
                  <td style="border: 1px solid black;">วันที่</td>
                  <td style="border: 1px solid black;">รายงาน</td>
                  <td style="border: 1px solid black;">รูปภาพ</td>
                  <td style="border: 1px solid black;">ผู้รายงาน</td>
                  <td style="border: 1px solid black;">หมายเหตุ</td>

                </tr>
                <tr>
                  <td style="border: 1px solid black;">&nbsp;</td>
                  <td style="border: 1px solid black;"></td>
                  <td style="border: 1px solid black;"></td>
                  <td style="border: 1px solid black;"></td>
                  <td style="border: 1px solid black;"></td>
                </tr>
            </table> --}}

              <div style="display: flex;justify-content: space-between;padding: 1%">
                    <div>
                        <p>ลงชื่อ .............................................. (ผู้ควบคุม)</p>
                        <p style="word-spacing: 6cm;"> (    )</p>
                        <p>ตำแหน่ง .............................................. </p>
                        <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;วันที่ ............................................... </p>
                    </div>
                    <div>
                        <p>ลงชื่อ .............................................. (ผู้รายงานซ่อมบำรุง)</p>
                        <p style="word-spacing: 6cm;"> (    )</p>
                        <p>ตำแหน่ง .............................................. </p>
                        <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;วันที่ ............................................... </p>
                    </div>
                  </div>
                </div>
    </body>
</html>
